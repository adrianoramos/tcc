using System;
using System.ComponentModel.DataAnnotations;

public class Ferias
{
	[Key]
	public int FeriasID {get; set;}

	[Required]
	[Display(Name = "Cliente")]
	public Cliente Cliente {get; set;}

	[Required]
	[Display(Name = "Per�odo")]
	public DateTime Periodo { get; set; }

	[Required]
	[Display(Name = "Quantidade de Dias")]
	public int QuantidadeDias {get; set;}

}

