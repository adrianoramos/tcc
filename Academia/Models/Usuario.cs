using System.ComponentModel.DataAnnotations;

public class Usuario : Cargo
{
	[Key]
	public int UsuarioID {get; set;}

	[Required]
	[Display(Name = "Cargo")]
	public Cargo Cargo {get; set;}

	[Required]
	[Display(Name = "Login")]
	public string Login {get; set;}

	[Required]
	[Display(Name = "Senha")]
	public string Senha {get; set;}

}

