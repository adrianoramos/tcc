using System;
using System.ComponentModel.DataAnnotations;

public class Cliente
{
	[Key]
	public int ClienteID { get; set; }

	[Required]
	[Display(Name = "Tipo de Aula")]
	public TipoAula TipoAula { get; set; }

	[Required]
	[Display(Name = "Forma de Pagamento")]
	public FormaPagamento FormaPagamento { get; set; }

	[Required]
	[Display(Name = "Plano")]
	public Plano Plano { get; set; }

	[Required]
	[Display(Name = "Aula")]
	public Aula Aula { get; set; }

	[Required]
	[Display(Name = "Matr�cula")]
	public int Matricula { get; set; }

	[Required]
	[Display(Name = "CPF")]
	public string CPF { get; set; }

	[Required]
	[Display(Name = "RG")]
	public string Rg { get; set; }

	[Display(Name = "Cliente")]
	public string Email { get; set; }

	[Required]
	[Display(Name = "Endereco")]
	public string Endereco { get; set; }

	[Required]
	[Display(Name = "Bairro")]
	public string Bairro { get; set; }

	[Required]
	[Display(Name = "Cidade")]
	public string Cidade { get; set; }

	[Required]
	[Display(Name = "Estado")]
	public string Estado { get; set; }

	[Required]
	[Display(Name = "�nicio do Contrato")]
	public DateTime InicioContrato { get; set; }

	[Required]
	[Display(Name = "Fim do Contrato")]
	public DateTime FimContrato { get; set; }

	[Required]
	[Display(Name = "Valor")]
	public decimal Valor { get; set; }

	[Required]
	[Display(Name = "N�mero de Parcelas")]
	public int Parcelas { get; set; }
}

