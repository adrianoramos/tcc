using System;
using System.ComponentModel.DataAnnotations;

public class Financeiro
{
	[Key]
	public int FinanceiroID {get; set;}

	[Required]
	[Display(Name = "Cliente")]
	public Cliente Cliente { get; set; }

	[Required]
	[Display(Name = "Plano")]
	public Plano Plano { get; set; }

	[Required]
	[Display(Name = "Forma de Pagamento")]
	public FormaPagamento FormaPagamento { get; set; }

	[Required]
	[Display(Name = "Valor Total")]
	public decimal ValorTotal {get; set;}

	[Display(Name = "Valor da Parcela")]
	public decimal ValorParcela {get; set;}

	[Display(Name = "N�mero de Parcelas")]
	public int NumeroParcela {get; set;}

	[Display(Name = "Data de Vencimento")]
	public DateTime Vencimento {get; set;}

	[Required]
	[Display(Name = "Data de Pagamento")]
	public DateTime DataPagamento {get; set;}

	[Required]
	[Display(Name = "Valor Pago")]
	public decimal ValorPago {get; set;}
}

