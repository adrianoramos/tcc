using System;
using System.ComponentModel.DataAnnotations;

public class Avaliacao
{
	[Key]
	public int AvaliacaoID { get; set; }

	[Required]
	[Display(Name = "Fisioterapeuta")]
	public Fisioterapeuta Fisioterapeuta { get; set; }

	[Required]
	[Display(Name = "Cliente")]
	public Cliente Cliente { get; set; }

	[Required]
	[Display(Name = "Anamnese")]
	public string Anamnese { get; set; }

	[Required]
	[Display(Name = "Cut�nea")]
	public string Cutanea { get; set; }

	[Required]
	[Display(Name = "Exame Ergom�trico")]
	public string ExameErgometrico { get; set; }

	[Required]
	[Display(Name = "Data da Avalia��o")]
	public DateTime DataAvaliacao { get; set; }

}

