using System.ComponentModel.DataAnnotations;

public class Instrutor
{
	[Key]
	public int InstrutorID {get; set;}

	[Required]
	[Display(Name = "Nome")]
	public string NomeInstrutor {get; set;}

	[Required]
	[Display(Name = "CPF")]
	public string Cpf {get; set;}

	[Required]
	[Display(Name = "RG")]
	public string Rg {get; set;}

	[Required]
	[Display(Name = "Tipo de Aula")]
	public TipoAula TipoAula {get; set;}
}

