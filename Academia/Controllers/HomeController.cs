﻿using Academia.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Academia.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var db = new AcademiaContext();

            var tipoAula = new TipoAula() { 
                DescricaoTipoAula = "Ginástica"
            };

            db.Entry(tipoAula).State = EntityState.Added;
            db.SaveChanges();

            return View();
        }
    }
}