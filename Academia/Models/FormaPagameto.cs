using System.ComponentModel.DataAnnotations;

public class FormaPagamento
{
	[Key]
	public int FormaPagamentoID { get; set;}

	public string DescricaoPagamento {get; set;}

}

