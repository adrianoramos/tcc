﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Academia.Models;

namespace Academia.Controllers
{
    public class InstrutorsController : Controller
    {
        private AcademiaContext db = new AcademiaContext();

        // GET: Instrutors
        public ActionResult Index()
        {
            return View(db.Instrutores.ToList());
        }

        // GET: Instrutors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instrutor instrutor = db.Instrutores.Find(id);
            if (instrutor == null)
            {
                return HttpNotFound();
            }
            return View(instrutor);
        }

        // GET: Instrutors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Instrutors/Create
        // Para se proteger de mais ataques, habilite as propriedades específicas às quais você quer se associar. Para 
        // obter mais detalhes, veja https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InstrutorID,NomeInstrutor,Cpf,Rg")] Instrutor instrutor)
        {
            if (ModelState.IsValid)
            {
                db.Instrutores.Add(instrutor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(instrutor);
        }

        // GET: Instrutors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instrutor instrutor = db.Instrutores.Find(id);
            if (instrutor == null)
            {
                return HttpNotFound();
            }
            return View(instrutor);
        }

        // POST: Instrutors/Edit/5
        // Para se proteger de mais ataques, habilite as propriedades específicas às quais você quer se associar. Para 
        // obter mais detalhes, veja https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InstrutorID,NomeInstrutor,Cpf,Rg")] Instrutor instrutor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(instrutor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(instrutor);
        }

        // GET: Instrutors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instrutor instrutor = db.Instrutores.Find(id);
            if (instrutor == null)
            {
                return HttpNotFound();
            }
            return View(instrutor);
        }

        // POST: Instrutors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Instrutor instrutor = db.Instrutores.Find(id);
            db.Instrutores.Remove(instrutor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
