using System.ComponentModel.DataAnnotations;

public class Fisioterapeuta : Usuario
{
	[Key]
	public int FisioterapeutaID {get; set;}

	[Required]
	[Display(Name = "Usu�rio")]
	public Usuario Usuario {get; set;}

	[Required]
	[Display(Name = "Nome")]
	public string NomeFisioterapeuta {get; set;}

}

