using System.ComponentModel.DataAnnotations;

public class TipoAula
{
	[Key]
	public int TipoAulaID {get; set;}

	public string DescricaoTipoAula {get; set;}

}

