using System.ComponentModel.DataAnnotations;

public class Cargo
{
	[Key]
	public int CargoID { get; set; }

	[Required]
	[Display(Name = "Descri��o")]
	public string DescricaoCargo { get; set; }

}