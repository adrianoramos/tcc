using System;
using System.ComponentModel.DataAnnotations;

public class Aula
{
	[Key]
	public int AulaID { get; set; }

	[Required]
	[Display(Name = "Tipo de Aula")]
	public TipoAula TipoAula { get; set; }

	[Required]
	[Display(Name = "Descri��o")]
	public string DescricaoAula { get; set; }

	[Required]
	[Display(Name = "Instrutor")]
	public string NomeInstrutor { get; set; }

	[Required]
	[Display(Name = "Hora �nicio")]
	public DateTime HoraInicio { get; set; }

	[Required]
	[Display(Name = "Hora t�rmino")]
	public DateTime HoraTermino { get; set; }

	[Required]
	[Display(Name = "Dias")]
	public string Dias { get; set; }

	[Required]
	[Display(Name = "Sala")]
	public string Sala { get; set; }

}

