﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Academia.Models
{
    public class AcademiaContext : DbContext
    {
        public DbSet<TipoAula> TiposAula { get; set; }
        public DbSet<Plano> Planos { get; set; }
        public DbSet<FormaPagamento> FormasPagamento { get; set; }
        public DbSet<Financeiro> Financeiro { get; set; }
        public DbSet<Cargo> Cargos { get; set; }
        public DbSet<Avaliacao> Avaliacoes { get; set; }
        public DbSet<Ferias> Ferias { get; set; }
        public DbSet<Aula> Aulas { get; set; }
        public DbSet<Fisioterapeuta> Fisioterapeutas { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Instrutor> Instrutores { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }


        public AcademiaContext() : base("Academia")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<AcademiaContext>());
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}